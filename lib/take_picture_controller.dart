import 'dart:async';
import 'dart:convert';

import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:upload_app/take_picture_model.dart';
import 'package:rxdart/rxdart.dart';
import 'package:http/http.dart' as http;

class TakePictureController {
  TakePictureModel model;
  Timer timer;
  TextEditingController addressTextController = TextEditingController();
  CameraController cameraController;
  Future<void> initializeControllerFuture;

  BehaviorSubject<TakePictureModel> takePictureModel = BehaviorSubject();

  Observable<TakePictureModel> get takePictureModelObservable =>
      takePictureModel.stream;

  startStopTakingImages() async {
    changeToogle();
    if (timer != null) {
      timer.cancel();
      timer = null;
    }
    if (model.takeImages) {
      await takeImage();
      takePictureModel.add(model);
      await uploadImage();
      timer =
          new Timer.periodic(Duration(seconds: model.getInterval()), (timer) async {
        await takeImage();
        takePictureModel.add(model);
        await uploadImage();
      });
      ;
    }
  }

  Future uploadImage() async {
    if (model.getImage() != null && await model.getImage().exists()) {
      String base64Image = base64Encode(model.getImage().readAsBytesSync());
      http
          .post(model.URL + model.getAddress().replaceAll(" ", "_") + ".jpg",
          body: base64Image)
          .then((res) {
        print(res.statusCode);
      }).catchError((err) {
        print(err);
      });
    }
  }

  void changeToogle() {
    model.takeImages = !model.takeImages;
    takePictureModel.add(model);
  }

  takeImage() async {
    try {
      await initializeControllerFuture;
      try {
        model.getImage().delete();
        imageCache.clear();
      } catch (e) {}
      try {
        print("image_path: " + model.getImage().path);
        await cameraController.takePicture(model.getImagePath());
      } catch (e) {}
      cameraController.initialize();
    } catch (e) {
      print(e);
    }
  }

  initModel(TakePictureModel model) {
    this.model = model;
    takePictureModel.add(model);
  }

  void setAddress(String text) {
    model.address = text;
  }

  void setInterval(int interval) {
    model.interval = interval;
  }

  dispose() {
    takePictureModel.close();
  }
}
