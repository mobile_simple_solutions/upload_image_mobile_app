import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:upload_app/take_picture_controller.dart';
import 'package:upload_app/take_picture_model.dart';
import 'package:flutter/services.dart';
void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MaterialApp(
      title: 'Upload',
      home: TakePictureScreen(),
    );
  }
}

class TakePictureScreen extends StatefulWidget {
  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  TextEditingController _addressTextController = TextEditingController();
  TextEditingController _intervalTextController = TextEditingController();
  TakePictureController controller = new TakePictureController();

  @override
  void initState() {
    super.initState();
    getTemporaryDirectory().then((onValue) {
      TakePictureModel model = new TakePictureModel();
      model.temporaryDirectoryPath = onValue.path;
      controller.initModel(model);
    });
    availableCameras().then((cameraList) {
      controller.cameraController = CameraController(
        cameraList.first,
        ResolutionPreset.medium,
      );
      controller.initializeControllerFuture =
          controller.cameraController.initialize();
    });
  }

  @override
  void dispose() {
    controller.cameraController.dispose();
    _addressTextController.dispose();
    _intervalTextController.dispose();
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: controller.takePictureModelObservable,
        builder: (context, snapshot) {
          TakePictureModel model;
          if (snapshot.hasData) {
            model = snapshot.data;
          }
          return Scaffold(
              appBar: AppBar(
                title: Text('Hakaton App'),
              ),
              body: Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      verticalDirection: VerticalDirection.down,
                      children: [
                        Container(
                          width: 300.0,
                          child: TextField(
                              enabled: !model.isTakingImages(),
                              decoration: new InputDecoration(
                                  hintText: "Enter Shop/Market address",
                                  labelText: "Enter Shop/Market address"),
                              controller: _addressTextController),
                        ),
                        Container(
                          width: 300.0,
                          child: TextField(
                              enabled: !model.isTakingImages(),
                              decoration: new InputDecoration(
                                  hintText:
                                      "Set interval in secconds, default is: " +
                                          model.getInterval().toString(),
                                  labelText: "Set interval in secconds"),
                              keyboardType: TextInputType.number,
                              controller: _intervalTextController),
                        ),
                        Expanded(child: CameraImage(model))
                      ])),
              floatingActionButton: new StartStopUpload(
                controller: controller,
                model: model,
                onPressed: () {
                  controller.setAddress(_addressTextController.text);
                  controller.startStopTakingImages();
                  controller
                      .setInterval(int.parse(_intervalTextController.text));
                },
              ));
        });
  }
}

class StartStopUpload extends StatelessWidget {
  const StartStopUpload(
      {@required this.controller,
      @required this.model,
      @required this.onPressed});

  final Function onPressed;
  final TakePictureController controller;
  final TakePictureModel model;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.bottomCenter,
          child: FloatingActionButton(
              onPressed: onPressed,
              child: model.takeImages
                  ? Icon(Icons.pause)
                  : Icon(Icons.play_arrow)),
        ),
      ],
    );
  }
}

class CameraImage extends StatelessWidget {
  final TakePictureModel model;

  const CameraImage(this.model);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        builder: (context, snapshot) {
          var fileExists = snapshot.data;
          if (fileExists) {
            if (model != null && model.getImage() != null) {
              var image = Image.file(model.getImage());
              return image;
            } else {
              return Container(
                  margin: const EdgeInsets.only(top: 100.0),
                  child: Text(
                      'Hello, This app is used in a project that analyses the number of people at a perticular supermarket, shop or random location, '
                      'The app sends periodicaly a photo and its location to a server. The server analuses the photo and counts the people in this location. To use add the location addres, add the interval and press the play button',
                      textAlign: TextAlign.center));
            }
          } else {
            return Container(
                margin: const EdgeInsets.only(top: 100.0),
                child: Text(
                    'Hello, This app is used in a project that analyses the number of people at a perticular supermarket, shop or random location. '
                    'The app sends periodicaly a photo and its location to a server. The server analuses the photo and counts the people in this location. To use add the location addres, add the interval and press the play button',
                    textAlign: TextAlign.center));
            ;
          }
        },
        future: model.getImage().exists());
  }
}
