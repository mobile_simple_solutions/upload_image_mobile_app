import 'dart:io';

import 'package:path/path.dart';

class TakePictureModel {
  bool takeImages = false;
  String temporaryDirectoryPath;
  String address;
  String URL =
      "https://bvahz6u658.execute-api.eu-central-1.amazonaws.com/Prod/api/file/";
  int _imagePrefix = 0;
  int interval;

  int getInterval() {
    if (interval == null || interval <= 0) {
      return 1;
    }
    return interval;
  }

  String getAddress() {
    if (address == null || address.isEmpty) {
      return "Random shop addres in Germany number: 10";
    }
    return address;
  }


  File getImage() {
    return File(_getLocalImagePath());
  }

  String getImagePath() {
    _imagePrefix = (_imagePrefix + 1) % 2;
    return _getLocalImagePath();
  }

  bool isTakingImages() {
    return takeImages;
  }

  String _getLocalImagePath() {
    return join(
      temporaryDirectoryPath,
      _imagePrefix.toString() + 'imageOfObject.jpg',
    );
  }

}
